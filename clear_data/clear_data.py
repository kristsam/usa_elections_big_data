import csv
import pandas as pd
import os
import numpy as np


def delete_col(path,columns_to_delete):
    '''deletes in given directory the given columns'''
    print("Deleting columns "+str(columns_to_delete)+" from file "+path.split('\\')[-1])
    data = pd.read_csv(path,encoding='utf-8',lineterminator='\n')
    data = data.drop(columns_to_delete, axis=1)
    data.to_csv(path, index=False, encoding='utf-8',line_terminator='\n')
    print("Columns deleted!")


def createUnifiedCSV(paths,new_file_path):
    '''creates one CSV for all data containing hashtags'''
    li = []
    for path in paths:
        print("Extracting data from file "+path.split('\\')[-1])
        data = pd.read_csv(path,encoding='utf-8',lineterminator='\n')
        data.insert(3, column = "hashtag", value = (path.split('\\')[-1])[:-4])  
        data.head()
        li.append(data)
        print("Data added!")
    frame = pd.concat(li, axis=0, ignore_index=True)
    frame.to_csv(new_file_path, index=False, encoding='utf-8',line_terminator='\n')

    print("File " +new_file_path.split('\\')[-1]+" created!")


def createSpecificCSV(path,to_attach):
    '''splits files given name and column'''
    print("Creating CSV file "+to_attach[0].split('\\')[-1]+" from file "+path.split('\\')[-1])
    data = pd.read_csv(path,encoding='utf-8',lineterminator='\n')
    data = data[to_attach[1]]
    data.to_csv(to_attach[0], index=False, encoding='utf-8',line_terminator='\n')
    print("File " +to_attach[0].split('\\')[-1]+" created!")

 
def removeEmptyRowsDuplicatesNA(path):
    '''removes empty rows, duplicates and replaces empty fields with N/A'''
    print("Removing duplicates and replacing empty fields with N/A from file "+path.split('\\')[-1])
    data = pd.read_csv(path,encoding='utf-8',lineterminator='\n')
    # all values have to be null to drop 
    data = data.dropna(axis = 0, how ='all')
    data = data.fillna(value='N/A', axis=0)
    data = data.drop_duplicates()
    data.to_csv(path, index=False, encoding='utf-8',line_terminator='\n')
    print("Operation successful. File saved!")


def changeDataTypes(path, float_to_int=[], object_to_datetime=[]):
    '''changes type in path for columns given  float64 -> int64, object -> datetime'''
    print("Changining data types in file "+path.split('\\')[-1])
    data = pd.read_csv(path,encoding='utf-8',lineterminator='\n')
    for obj in float_to_int:
        data[obj] = data[obj].astype('int64')
    for obj in object_to_datetime:
        data[obj] =  pd.to_datetime(data[obj])
    print("New data types:")
    print(data.dtypes)
    data.to_csv(path, index=False, encoding='utf-8',line_terminator='\n')

def removeDuplicatesSpecificKeys(path, keys):
    '''removes duplicates values given path and they key columns keeping the last element saw'''
    print("Deleting duplicates from file "+path.split('\\')[-1])
    data = pd.read_csv(path,encoding='utf-8',lineterminator='\n')
    data = data.drop_duplicates(subset=keys, keep='last')
    data.to_csv(path, index=False, encoding='utf-8',line_terminator='\n')
    print("Operation successful. File saved!")

def createMonthYearColumn(path, datetime_column):
    '''creates month, year columns in existing file with values based on datetime'''
    print('Creating month, year columns in file '+path.split('\\')[-1])
    data = pd.read_csv(path,encoding='utf-8',lineterminator='\n')
    data['month'] = pd.DatetimeIndex(data['created_at']).month
    data['year'] = pd.DatetimeIndex(data['created_at']).year
    data.to_csv(path, index=False, encoding='utf-8',line_terminator='\n')
    print("Columns created. File saved!")

def replaceComma(path, columns, char):
    '''replaces comma in given CSV file, in given columns with the char given'''
    print('Replacing "," with "'+char+'" in file '+path.split('\\')[-1])
    data = pd.read_csv(path,encoding='utf-8',lineterminator='\n')
    for column in columns:
        data [column] = data[column].str.replace(",",char)
    data.to_csv(path, index=False, encoding='utf-8',line_terminator='\n')
    print("Comma replaced. File saved!")

def replaceHashtagColWithTwoBool(path):
    '''creates two new boolean columns and deletes old hashtag column'''
    print('Replacing column "hashtag" with boolean "hashtag_donaldtrump" and "hashtag_joebiden" in file '+path.split('\\')[-1])
    data = pd.read_csv(path,encoding='utf-8',lineterminator='\n')
    data['hashtag_donaldtrump'] = data.apply (lambda row: isTrump(row), axis=1)
    data['hashtag_joebiden'] = data.apply (lambda row: isBiden(row), axis=1)
    data = data.drop('hashtag', axis=1)
    data.to_csv(path, index=False, encoding='utf-8',line_terminator='\n')
    print("Column replaced. File saved!")

def isTrump (row):
   if row['hashtag'] == 'hashtag_donaldtrump' :
      return True
   return False

def isBiden (row):
   if row['hashtag'] == 'hashtag_joebiden' :
      return True
   return False

def concatenateDuplicatesFinishesEdit(path, keys):
    '''removes duplicates by saving tweet with both hashtags as one'''
    print("Concatenating duplicates from file "+path.split('\\')[-1])
    data = pd.read_csv(path,encoding='utf-8',lineterminator='\n')
    print(data.shape[0])
    data.loc[data.duplicated(subset=keys, keep=False),['hashtag_donaldtrump', 'hashtag_joebiden']] = [True, True]
    data = data.drop_duplicates(keys)
    print(data.shape[0])
    # free text contains change line values, we make them "~"
    data['tweet'] = data['tweet'].replace('\n', '~', regex=True)
    data['tweet'] = data['tweet'].replace('\r\n', '~', regex=True)
    data['tweet'] = data['tweet'].replace('\r', '~', regex=True)
    data['tweet'] = data['tweet'].replace('\036', '~', regex=True)
    data['tweet'] = data['tweet'].replace('\n\r', '~', regex=True)
    data['tweet'] = data['tweet'].replace('\025', '~', regex=True)
    data['tweet'] = data['tweet'].replace(r'\\n','~', regex=True)
    
    data['created_at'] =  pd.to_datetime(data['created_at'])
    print(data.describe())
    print(data.dtypes)
    data.to_csv(path, index=False, encoding='utf-8',line_terminator='\n')
    print("Operation successful. File saved!")

    

path_to_data = os.path.dirname(os.path.realpath(__file__))+'\\..\\data\\'
paths = [path_to_data+'hashtag_donaldtrump.csv',path_to_data+'hashtag_joebiden.csv']
columns_to_delete = ["source", "user_name", "user_screen_name", "user_description", "user_join_date", "user_location", "lat", "long", "collected_at" ]

for path in paths:
    delete_col(path, columns_to_delete)

unified_path = path_to_data+"unified_data.csv"

createUnifiedCSV(paths,unified_path)

to_attach_list = [[path_to_data+"tweets_fact_table.csv",['tweet_id','tweet','hashtag','created_at','user_id','likes','retweet_count']], [path_to_data+"time_info.csv",['created_at']], [path_to_data+"t_user.csv",['user_id','user_followers_count','city','state_code','state','country','continent']]]

for to_attach in to_attach_list: 
    createSpecificCSV(unified_path,to_attach)
    removeEmptyRowsDuplicatesNA(to_attach[0])

changeDataTypes(to_attach_list[0][0], float_to_int=['tweet_id','user_id','likes','retweet_count'], object_to_datetime=['created_at'])
changeDataTypes(to_attach_list[1][0], object_to_datetime=['created_at'])
changeDataTypes(to_attach_list[2][0], float_to_int=['user_id','user_followers_count'])

removeDuplicatesSpecificKeys(to_attach_list[2][0], ['user_id'])

createMonthYearColumn(to_attach_list[1][0], 'created_at')

replaceComma(to_attach_list[0][0], ['tweet'], '|')

replaceHashtagColWithTwoBool(to_attach_list[0][0])

concatenateDuplicatesFinishesEdit(to_attach_list[0][0],['tweet_id'])
