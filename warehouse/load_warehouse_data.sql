BULK INSERT T_User
 FROM 'C:\Users\Christos\Desktop\MyProjects\usa_elections_big_data\data\t_user.csv'
WITH (FIRSTROW =2, FIELDTERMINATOR= ',', ROWTERMINATOR = '0x0a', CODEPAGE = '65001', DATAFILETYPE = 'Char');

BULK INSERT Time_Info
 FROM 'C:\Users\Christos\Desktop\MyProjects\usa_elections_big_data\data\time_info.csv'
WITH (FIRSTROW =2, FIELDTERMINATOR= ',', ROWTERMINATOR = '0x0a', CODEPAGE = '65001', DATAFILETYPE = 'Char');

BULK INSERT Tweets_Fact_Table
 FROM 'C:\Users\Christos\Desktop\MyProjects\usa_elections_big_data\data\tweets_fact_table.csv'
WITH (FIRSTROW =2, FIELDTERMINATOR= ',', ROWTERMINATOR = '0x0a', CODEPAGE = '65001', DATAFILETYPE = 'Char');
