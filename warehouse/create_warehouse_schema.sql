-- warehouse schema

CREATE TABLE T_User
	(user_id bigint primary key,
	user_followers_count int,
	city nvarchar(50),
	state_code nvarchar(10),
	state nvarchar(50),
	country nvarchar(50),
	continent nvarchar(50),
	);

CREATE TABLE Time_Info
	(t_date datetime primary key,
	month int,
	year int,
	);

CREATE TABLE Tweets_Fact_Table
	(tweet_id bigint primary key,
	tweet nvarchar(2000),
	t_date datetime,
	user_id bigint,
	likes bigint,
	retweet_count bigint,
	hashtag_donaldtrump nvarchar(20),
	hashtag_joebiden nvarchar(20),

	foreign key (t_date) references Time_Info(t_date),
	foreign key (user_id) references T_User(user_id),
	);

ALTER TABLE Tweets_Fact_Table
ALTER COLUMN hashtag_donaldtrump bit
ALTER TABLE Tweets_Fact_Table
ALTER COLUMN hashtag_joebiden bit;

-- important changes

UPDATE T_User
SET city='N/A'
WHERE city IS NULL;

UPDATE T_User
SET country='N/A'
WHERE country IS NULL;

UPDATE T_User
SET state='N/A'
WHERE state IS NULL;

UPDATE T_User
SET state_code='N/A'
WHERE state_code IS NULL;

UPDATE T_User
SET continent='N/A'
WHERE continent IS NULL;

UPDATE T_User
SET continent='Europe', country='Greece'
WHERE continent ='Greece,Europe'

UPDATE T_User
SET country='United States of America'
WHERE country='United States'

UPDATE T_User
SET country='The Netherlands'
WHERE country='Netherlands'

UPDATE T_User
SET country='The Bahamas'
WHERE country='Bahamas'

UPDATE Tweets_Fact_Table
SET tweet = REPLACE(tweet, '|', ',')

-- data CUBE

SELECT t.month, u.country, count(*) total_tweets, sum(sign(f.hashtag_donaldtrump)) AS containg_h_DonaldTrump, sum(sign(f.hashtag_joebiden)) AS containing_h_JoeBiden, sum(likes) AS likes, sum(retweet_count) AS retweets
FROM Tweets_Fact_Table as f, Time_Info as t, T_User as u
WHERE f.t_date = t.t_date and f.user_id = u.user_id
GROUP BY CUBE(t.month, u.country)